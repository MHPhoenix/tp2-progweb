<!DOCTYPE html>

<html lang="fr">

	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="../css/design.css">
		<link rel="stylesheet" type="text/css" href="../css/enrolement.css">
		<link rel="stylesheet" style="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../JS/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="../JS/jstree.min.js"></script>
		<title>CENI/DepotDeCandidature</title>
</head>

	<body>

		<div>
				<a href="../html/pageAcceuil.html"><img src="../images/logo.jpg"
					class="logo"></a> <i class="slogan"> J'arr�te de polluer, je sauve ma
					Terre! </i> <span class="glyphicon glyphicon-envelope"><br /> <br />
				<a class="lien" href="mailto:ceni@uqam.ca"> ceni@uqam.ca </a> </span>
					<span class="glyphicon glyphicon-earphone"><br /> <br /> 514.001.0023
					</span> <span class="glyphicon glyphicon-globe"> <br /> 0102 Rue
					Terre-Vivante <br /> T2M1C0
					</span> <span class="glyphicon glyphicon-book"> <br /> <br /> <a
					class="lien" href="../html/pageAcceuil.html"> Francais </a> <a
					class="lien" href="../html/anglais.html"> Anglais </a></span>
		</div><br/>
		
		<nav role="navigation">

			<ul class="barre">
				<li><a href="../html/contacts.html">Contacts</a></li>
				<li><a href="../html/enrolement.html">Enrolement</a></li>
				<li><a href="../html/connexion.html">Connexion</a></li>
				<li><a href="../html/carteElec.html">Carte Electorale</a></li>
				<li><a href="../html/publication.html">Publications</a></li>
				<li><a href="../html/partis.html">Partis</a></li>
				<li><a href="../php/candidats.php">Candidats</a></li>
				<li><a href="#!">Elections</a>
					<ul class="dropdown">
						<li><a href="../html/electionPresident.html">electionPresidentielle</a></li>
						<li><a href="../html/electionFederal.html">electionFederale</a></li>
						<li><a href="../html/electionProvincial.html">electionProvinciale</a></li>
					</ul></li>
			</ul>

		</nav>
		
		<div class="container">
		
		<?php
	       require 'config.php';
    	?>
    	
    	<h2>Depot de Candidature</h2>

		<form method="post" action="../php/depot.php"
			enctype="multipart/form-data">

			<fieldset>
				<legend>S�lectionner niveau electoral</legend>
				<label for="niveauid">Niveaux Electoral </label> 
				<select name="niveau" id="niveauid" class="form-control" required="required">
					<option>S�lectionner</option>
					<option value="P">Election Presidentielle</option>
					<option value="DN">Election nationalle (deputes)</option>
					<option value="DP">Election Provinciale (deputes)</option>
				</select>
			</fieldset>

			<fieldset class="form-group">
				<legend>S'Identifier</legend>
				<div class="form-group">
					<label for="nomid" class="form-label">Nom: </label> <input
						type="text" name="nom" id="nomid" class="form-control"
						required="required">
				</div>
				<div class="form-group">
					<label for="prenomid">Pr�nom: </label> <input type="text"
						name="prenom" id="prenomid" class="form-control"
						required="required">
				</div>
				<div class="form-group">
					<label for="dateid">Date de Naissance:</label> <input type="date"
						name="datenaissance" id="dateid" class="form-control"
						required="required">
				</div>
			</fieldset>

			<fieldset>
				<legend>D�poser une photo</legend>
				<div class="form-group">
					<label for="fichierid" class="form-label col-sm-2">Fichier image:</label>
					<input type="file" name="fichier" class="form-control-file col-sm-4"
						id="fichierid" required="required" style="color: transparent;" />
				</div>
			</fieldset>

			<div>
				Parti/Regroupement Politique : <br/>
				
			<?php
                    global  $database;
                    $enregistrements = $database->select("partis", [
                        "id",
        				"nom",]);
            ?>
            </div>
            
			<select name="parti" required="required">
			<?php
                foreach($enregistrements as $enregistrement){
			?>
			
			<option value='<?=$enregistrement["id"]?>'><?=$enregistrement["nom"]?></option>
				<?php 		
                }
			?>
			</select>
			
			<div>
				Circonscription electorale : <br>
					
				<select name="circonscription" required="required">

					        <option value='P0'>Canada</option>
					   
					<optgroup label='Deputes Nationaux'>
			
						<option value='DN1'>Edmonton</option>
						<option value='DN2'>Ottawa</option>
						<option value='DN3'>Quebec</option>
						
					</optgroup>
					
					<optgroup label='Deputes Provinciaux'>
		
						<option value='DP1'>Alberta</option>
						<option value='DP2'>Ontario</option>
						<option value='DP3'>Toronto</option>
							
					</optgroup>
				</select>
				<br>
			</div>
	
	
			<div>
				Autres Informations : <br>
				<h3>Election Presidentielle</h3>
				<br> <input type="checkbox" value="Circonscription 1"> Avoir minimum
				25 ans <br> <input type="checkbox" value="Circonscription 2"> Etre
				membre ressortissant d'un des pays formant la CENI <br> <input
					type="checkbox" value="Document 1"> Acte de naissance <br> <input
					type="checkbox" value="Document 2"> Preuve de residence <br> <input
					type="checkbox" value="Document 3"> Preuve d'oeuvres sociales <br>

				<h3>Election nationalle (deputes)</h3>
				<br> <input type="checkbox" value="Circonscription 1"> Avoir minimum
				25 ans <br> <input type="checkbox" value="Circonscription 2"> Etre
				resident permanent du Canada <br> <input type="checkbox"
					value="Document 1"> Acte de naissance <br> <input type="checkbox"
					value="Document 2"> Preuve de residence <br> <input type="checkbox"
					value="Document 3"> Preuve d'oeuvres sociales <br>

				<h3>Election Provinciale (deputes)</h3>
				<br> <input type="checkbox" value="Circonscription 1"> Avoir minimum
				25 ans <br> <input type="checkbox" value="Circonscription 2"> Ne pas
				�tre sous curatelle ou avoir perdu ses droits �lectoraux <br> <input
					type="checkbox" value="Document 1"> Acte de naissance <br> <input
					type="checkbox" value="Document 2"> Preuve de residence <br> <input
					type="checkbox" value="Document 3"> Preuve d'oeuvres sociales <br>
			</div>
			
			<div class="submitreset">
				<input type="reset" value="Annuler"> <input type="submit"
					value="Soumettre">
			</div>

			</form>

		</div>
        
        <footer>
			<hr/>
			<div class="Merveille">
				<address>
					<a class="lien" href="mailto:mongbomerveille@yahoo.com"> MONGBO
						Houefa Orphyse Peggy Merveille </a>
				</address>
			</div>

			<div class="Corneille">
				<address>
					<a class="lien" href="mailto:corneillekouassivi@gmail.com">
						KOUASSIVI Corneille Frejus Codjovi </a>
				</address>
			</div>

			<hr/>
				<div>
					<address>
						Ce site est developpe dans le cadre du cours <a class="lien"
							href="http://www.etudier.uqam.ca/cours?sigle=INF2005"
							target="-blanck"> INF2005 </a> a l' <a class="lien"
							href="https://www.youtube.com/watch?v=X5DWqN3Xu-0"> UQAM </a>
					</address>
				</div>
		</footer>
	
	</body>

</html>	