<!DOCTYPE html>

<html lang = "fr">

<head>

<base href = "../html/pageAcceuil" target = "-blanck">
<meta charset = "UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel = "stylesheet" type = "text/css" href = "../css/design.css">
<link rel = "stylesheet" type = "text/css" href = "../css/bootstrap.min.css">
<link rel = "stylesheet" type = "text/css" href = "//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../JS/jquery-3.3.1.js"></script>
<script type = "text/javascript" src = "../JS/jquery.dataTables.min.js"></script>

<script>
function initDatatable(ajaxurl){
	// cr�ation datatable
	$("#candidtab").show();
	$("#candidtab").DataTable({
		"processing":true,
		ajax:"../php/candidats.php?search=P",
		columns:[
		{data:"niveau"},
		{data:"nom"},
		{data:"prenom"},
		{data:"datenaissance"},
		{data:"photo"},
		{data:"parti"},
		{data:"circonscription"}
		]
	    
	});
}

function reloadDatatable(ajaxurl){
	ajaxurl= ajaxurl.trim();
	if ( ! $.fn.DataTable.isDataTable( '#candidtab' ) ) {
		// appeler la fonction pour cr�er datatable
		initDatatable(ajaxurl);
		}else{//recharger les donn�es
			$("#candidtab").DataTable().ajax.url( ajaxurl ).load();
		}
		
}

$(document).ready(function(){
	$("#candidtab").hide();
	$("#P").click(function(){
		//retourne les candidats de la presidentielle
		var ajaxurlpresidentiel ="../php/candidats.php?search=P";
		reloadDatatable(ajaxurlpresidentiel)
	});
	
	$("#DN").click(function(){
		//retourne les candidats des elections deputes nationaux
		var ajaxurlnationaux ="../php/candidats.php?search=DN";
		reloadDatatable(ajaxurlnationaux);
	});
	$("#DP").click(function(){
		//retourne les candidats des elections deputes provinciaux
		var ajaxurlprovinciaux ="../php/candidats.php?search=DP";
		reloadDatatable(ajaxurlprovinciaux);
	});
});

</script>
		<title>CENI/Candidats</title>
</head>

	<body>

<header> 

			<a href="../html/pageAcceuil.html"><img src = "../images/logo.jpg" class = "logo" ></a>
			<i class = "slogan"> J'arr�te de polluer, je sauve ma Terre! </i>
			<span class = "glyphicon glyphicon-envelope"><br/><br/> <a class = "lien" href = "mailto:ceni@uqam.ca"> ceni@uqam.ca </a> </span>
			<span class = "glyphicon glyphicon-earphone"><br/><br/> 514.001.0023 </span>
			<span class = "glyphicon glyphicon-globe"> <br/> 0102 Rue Terre-Vivante <br/> T2M1C0 </span>
			<span class = "glyphicon glyphicon-book"> <br/><br/> <a class= "lien" href = "../html/pageAcceuil.html">  Francais  </a> 
			<a class="lien" href = "../html/anglais.html">  Anglais  </a></span>
			
		</header>
		<br/>
		
		<nav role="navigation">
		
		<ul class="barre">
			<li><a href="../html/contacts.html">Contacts</a></li>
			<li><a href="../html/enrolement.html">Enrolement</a></li>
			<li><a href="../html/connexion.html">Connexion</a></li>
			<li><a href="../html/carteElec.html">Carte Electorale</a></li>
			<li><a href="../html/publication.html">Publications</a></li>
			<li><a href="../html/partis.html">Partis</a></li>
			<li><a href="../php/candidats.php">Candidats</a></li>
			<li><a href="#!">Elections</a>
			<ul class="dropdown">
				<li><a href="../html/electionPresident.html">electionPresidentielle</a></li> 
				<li><a href="../html/electionFederal.html">electionFederale</a></li> 
				<li><a href="../html/electionProvincial.html">electionProvinciale</a></li>
			</ul>
			</li>
		</ul>
		
		</nav>
		
		<div class="container">
		
		<?php
	
	       require 'config.php';
	       
	       function createImgTag($records){ // creation url img a partir du chemin fichier image
	           $recordsWithImg= array();
	           foreach ($records as $record){
	               $record['photo'] ="<img src='".$record['photo']."' style='width:100%;'/></div>";
	               $recordsWithImg[] = $record;
	           }
	           return ($recordsWithImg);
	       }
	       
	       
	       $returnval= array();
	       $enregistrements = $database->select("candidats", [
	           "niveau",
	           "nom",
	           "prenom",
	           "datenaissance",
	           "photo",
	           "parti",
	           "circonscription"
	       ]);
	       
	       if(strcasecmp ($_REQUEST['search'],'P') == 0){  
	           //retourne les candidats de la presidentielle
	           foreach ($enregistrements as $enregistrement){
	               if(strcasecmp($enregistrement['niveau'],'P') == 0){
	                   $returnval[] = $enregistrement;
	               }
	           }
	           
	       }elseif (strcasecmp ($_REQUEST['search'],'DN') == 0){
	           //retourne les candidats des elections deputes nationaux
	           foreach ($enregistrements as $enregistrement){
	               if(strcasecmp($enregistrement['niveau'],'DN') == 0){
	                   $returnval[] = $enregistrement;
	               }
	           }
	       }else{
	           //retourne les candidats des elections deputes provinciaux
	           foreach ($enregistrements as $universite){
	               if(strcasecmp($universite['niveau'],'DP') == 0){
	                   $returnval[] = $enregistrement;
	               }
	           }
	       }
	       
	       $returnval = createImgTag($returnval);// pour cr�er les tag img
	       $returnval = array('data'=>$returnval);
	       echo json_encode($returnval,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);   
	       ?>
	       
		<h4>Cliquer sur un bouton pour afficher la liste des candidats par niveaux electoral</h4>
		
		<button id="P" class=" btn btn-primary">Tous les candidats a la presidentielle</button>
		<button id="DN" class=" btn btn-primary">Tous les candidats deputes nationaux</button>
		<button id="DP" class=" btn btn-primary">Tous les candidats deputes provinciaux</button>
		
		<table  id="candidtab" class="display">
    		    
			<thead>
				<tr>
					<th>Niveau</th>
					<th>Nom</th>
					<th>Prenom</th>
					<th>Date de Naissance</th>
					<th>Photo</th>
					<th>Parti</th>
					<th>Circonscription</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Niveau</th>
					<th>Nom</th>
					<th>Prenom</th>
					<th>Date de Naissance</th>
					<th>Photo</th>
					<th>Parti</th>
					<th>Circonscription</th>
				</tr>
			</tfoot> 
			</table>
			</div>
			
		<footer>
			<hr/>
			<div class="Merveille">
				<address>
					<a class="lien" href="mailto:mongbomerveille@yahoo.com"> MONGBO
						Houefa Orphyse Peggy Merveille </a>
				</address>
			</div>

			<div class="Corneille">
				<address>
					<a class="lien" href="mailto:corneillekouassivi@gmail.com">
						KOUASSIVI Corneille Frejus Codjovi </a>
				</address>
			</div>

			<hr/>
				<div>
					<address>
						Ce site est developpe dans le cadre du cours <a class="lien"
							href="http://www.etudier.uqam.ca/cours?sigle=INF2005"
							target="-blanck"> INF2005 </a> a l' <a class="lien"
							href="https://www.youtube.com/watch?v=X5DWqN3Xu-0"> UQAM </a>
					</address>
				</div>
		</footer>
	
	</body>

</html>