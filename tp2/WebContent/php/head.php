<!DOCTYPE html>

<html lang = "fr">
		
	<head>
	
		<base href = "../html/pageAcceuil" target = "-blanck">
		<meta charset = "UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel = "stylesheet" type = "text/css" href = "../css/design.css">
		<link rel = "stylesheet" type = "text/css" href = "../css/bootstrap.min.css">
		<title> CENI </title>

	</head>

	<body>
	<header> 

			<a href="../html/pageAcceuil.html"><img src = "../images/logo.jpg" class = "logo" ></a>
			<i class = "slogan"> J'arr�te de polluer, je sauve ma Terre! </i>
			<span class = "glyphicon glyphicon-envelope"><br/><br/> <a class = "lien" href = "mailto:ceni@uqam.ca"> ceni@uqam.ca </a> </span>
			<span class = "glyphicon glyphicon-earphone"><br/><br/> 514.001.0023 </span>
			<span class = "glyphicon glyphicon-globe"> <br/> 0102 Rue Terre-Vivante <br/> T2M1C0 </span>
			<span class = "glyphicon glyphicon-book"> <br/><br/> <a class= "lien" href = "../html/pageAcceuil.html">  Francais  </a> 
			<a class="lien" href = "../html/anglais.html">  Anglais  </a></span>
			
		</header>
		<br/>
		
		<nav role="navigation">
		
		<ul class="barre">
			<li><a href="../html/contacts.html">Contacts</a></li>
			<li><a href="../html/enrolement.html">Enrolement</a></li>
			<li><a href="../html/connexion.html">Connexion</a></li>
			<li><a href="../html/carteElec.html">Carte Electorale</a></li>
			<li><a href="../html/publication.html">Publications</a></li>
			<li><a href="../html/partis.html">Partis</a></li>
			<li><a href="../php/candidats.php">Candidats</a></li>
			<li><a href="#!">Elections</a>
			<ul class="dropdown">
				<li><a href="../html/electionPresident.html">electionPresidentielle</a></li> 
				<li><a href="../html/electionFederal.html">electionFederale</a></li> 
				<li><a href="../html/electionProvincial.html">electionProvinciale</a></li>
			</ul>
			</li>
		</ul>
		
		</nav>
      <div class="container">
