<!DOCTYPE html>

<html lang = "fr">
		
	<head>
	
		<base href = "../html/pageAcceuil" target = "-blanck">
		<meta charset = "UTF-8">
		<meta http-equiv="refresh" content="7; URL=../php/candidats.php">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel = "stylesheet" type = "text/css" href = "../css/design.css">
		<link rel = "stylesheet" type = "text/css" href = "../css/bootstrap.min.css">
		<title> CENI </title>

	</head>

	<body>
	<header> 

			<a href="../html/pageAcceuil.html"><img src = "../images/logo.jpg" class = "logo" ></a>
			<i class = "slogan"> J'arr�te de polluer, je sauve ma Terre! </i>
			<span class = "glyphicon glyphicon-envelope"><br/><br/> <a class = "lien" href = "mailto:ceni@uqam.ca"> ceni@uqam.ca </a> </span>
			<span class = "glyphicon glyphicon-earphone"><br/><br/> 514.001.0023 </span>
			<span class = "glyphicon glyphicon-globe"> <br/> 0102 Rue Terre-Vivante <br/> T2M1C0 </span>
			<span class = "glyphicon glyphicon-book"> <br/><br/> <a class= "lien" href = "../html/pageAcceuil.html">  Francais  </a> 
			<a class="lien" href = "../html/anglais.html">  Anglais  </a></span>
			
		</header>
		<br/>
		
		<nav role="navigation">
		
		<ul class="barre">
			<li><a href="../html/contacts.html">Contacts</a></li>
			<li><a href="../html/enrolement.html">Enrolement</a></li>
			<li><a href="../html/connexion.html">Connexion</a></li>
			<li><a href="../html/carteElec.html">Carte Electorale</a></li>
			<li><a href="../html/publication.html">Publications</a></li>
			<li><a href="../html/partis.html">Partis</a></li>
			<li><a href="../php/candidats.php">Candidats</a></li>
			<li><a href="#!">Elections</a>
			<ul class="dropdown">
				<li><a href="../html/electionPresident.html">electionPresidentielle</a></li> 
				<li><a href="../html/electionFederal.html">electionFederale</a></li> 
				<li><a href="../html/electionProvincial.html">electionProvinciale</a></li>
			</ul>
			</li>
		</ul>
		
		</nav>
		
      <div class="container">
      
      <?php
      
      require 'config.php';
      
      class Depots {
          
          const ECHEC = 0;
          const SUCCES = 1;
          const PAS_DE_FORMULAIRE = 2;
          private $dossierImages = '.';
          
          protected function enregistreImage(){
              
              //enregistre une image et retourne le chemin
              if (!isset($_FILES["fichier"])) {
                  return self::ECHEC;
              }
              
              $infoFich = $_FILES["fichier"];
              $nomFichier = $infoFich['name']; // nom du fichier charg� par l,utilisateur
              $tmpFile=$infoFich['tmp_name'];// fichier temporaire apr�s t�l�versement
              $fichierEnregistre=$this->dossierImages."/$nomFichier";
              
              if(!file_exists($this->dossierImages)){
                  mkdir("".$this->dossierImages."") ||
                  die("<span style='color:red'>impossible de cr�er le dossier images: ".$this->dossierImages."</span>");
              }
              
              move_uploaded_file("$tmpFile","".$fichierEnregistre."") ||
              die("<span style='color:red'>impossible de sauvegarder $nomFichier  dans ".$this->dossierImages."</span>");
              return ($fichierEnregistre);
          }
          
          
          public function enregistrerForm() {
              
              global  $database;
              $infoForm = array();
              
              if (!isset($_REQUEST["soumettre"])) {
                  return self::PAS_DE_FORMULAIRE;
              }
              
              $infoForm["niveau"] = $_REQUEST["niveau"];
              $infoForm["nom"] = $_REQUEST["nom"];
              $infoForm["prenom"] = $_REQUEST["prenom"];
              $infoForm["datenaissance"] = $_REQUEST["datenaissance"];
              $infoForm["photo"] = $this->enregistreImage();
              $infoForm["parti"] = $_REQUEST["parti"];
              $infoForm["circonscription"] = $_REQUEST["circonscription"];
              
              $insertionOk = $database->insert("candidats",$infoForm);
              
              if ($insertionOk) {
                  return self::SUCCES;
                  echo "<h3 class='text-success'>Enregistr� avec succ�s!</h3>";
              } else {
                  echo "<h3 class='text-danger'>Echec enregistrement base de donn�es</h3>";
                  return self::ECHEC;
              }
          }
          
          function __construct($repImg) {
              
              if(empty($repImg)){
                  die("Il faut sp�cifier le dossier pour enregistrer images");
              }
              
              $this->dossierImages = $repImg;
          }
          
      }
      
      //---- manipulation classe --
      $imag = "../images";// dossier pour enregistrer les images
      $cand = new Depots($imag);
      $cand->enregistrerForm();
      
      ?>
      
      <h3>Patientez un instant je vous prie</h3>
</div>
		
		<footer>
			<hr/>
				<div class = "Merveille">
					<address> 
						<a class="lien" href = "mailto:mongbomerveille@yahoo.com"> MONGBO Houefa Orphyse Peggy Merveille </a>
					</address>
				</div>
			
				<div class = "Corneille">
					<address> 
						<a class="lien" href = "mailto:corneillekouassivi@gmail.com"> KOUASSIVI Corneille Frejus Codjovi </a>
					</address>
				</div>
			
			<hr/>
			
			<div>
				<address> 
					Ce site est developpe dans le cadre du cours 
					<a class="lien" href = "http://www.etudier.uqam.ca/cours?sigle=INF2005" target = "-blanck"> INF2005 </a> a l'
					<a class="lien" href = "https://www.youtube.com/watch?v=X5DWqN3Xu-0"> UQAM </a>
				</address>
			</div>

		</footer>

	</body>
	
</html>