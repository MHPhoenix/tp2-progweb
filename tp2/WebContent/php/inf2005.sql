-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 04, 2019 at 09:03 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inf2005`
--

-- --------------------------------------------------------

--
-- Table structure for table `tp_candidats`
--

CREATE TABLE IF NOT EXISTS `tp_candidats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `niveau` enum('P','DN','DP') NOT NULL,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `datenaissance` date NOT NULL,
  `photo` varchar(64) NOT NULL,
  `parti` int(11) NOT NULL,
  `circonscription` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tp_electeurs`
--

CREATE TABLE IF NOT EXISTS `tp_electeurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(25) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `telephone` varchar(25) DEFAULT NULL,
  `courriel` varchar(64) DEFAULT NULL,
  `adresse` varchar(100) NOT NULL,
  `commentaire` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tp_partis`
--

CREATE TABLE IF NOT EXISTS `tp_partis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sigle` varchar(6) NOT NULL,
  `nom` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
