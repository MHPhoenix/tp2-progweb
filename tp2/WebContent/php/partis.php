<?php

require 'head.php';
require 'config.php';

class Partis {
    const ECHEC = 0;
    const SUCCES = 1;
    
public function enregistrerForm() {
    
    global  $database;
    $infoForm = array();
    
    if(!isset($_REQUEST["soumettre"])){
        afficherTout();
    }
    
    $infoForm["sigle"] = $_REQUEST["sigle"];
    $infoForm["nom"] = $_REQUEST["nom"];
  
    // ici on a deja collect� toutes informations du formulaire
    // on va enregistrer dans la BD
    
    $insertionOk = $database->insert("partis",$infoForm);
    if($insertionOk){
        return self::SUCCES;
        echo "<h3 class='text-success'>Enregistr� avec succ�s!</h3>";
    }else{
        echo "<h3 class='text-danger'>Echec enregistrement base de donn�es</h3>";
        return self::ECHEC;
    }
    
}

public function afficherTout() {
    global  $database;
    $enregistrements = $database->select("partis", [
        "sigle",
        "nom",
    ]);
    
        echo "<h3>Liste des enregistrements</h2>";
        echo "<div class='container' style='text-color:black;background-color:white;'>";     
        echo "<table class='table'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Sigle</th>";
        echo "<th>Nom</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach ($enregistrements as $enregistrement){
        echo "<tr>";
        echo "<td>".$enregistrement["sigle"]."</td>";
        echo "<td>".$enregistrement["nom"]."</td>";
        echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
         
    } 
}

$part = new Partis;
$part->enregistrerForm();
$part->afficherTout();

require 'tail.php';