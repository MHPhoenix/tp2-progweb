<!DOCTYPE html>

<html lang = "fr">

<head>

<base href = "../html/pageAcceuil" target = "-blanck">
<meta charset = "UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel = "stylesheet" type = "text/css" href = "../css/design.css">
<link rel = "stylesheet" type = "text/css" href = "../css/bootstrap.min.css">
<link rel = "stylesheet" type = "text/css" href = "//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="../JS/jquery-3.3.1.js"></script>
<script type = "text/javascript" src = "../JS/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#tabElect').DataTable();
} );
</script>
<title> CENI </title>
    
    </head>
    
    <body>
    <header> 

			<a href="../html/pageAcceuil.html"><img src = "../images/logo.jpg" class = "logo" ></a>
			<i class = "slogan"> J'arr�te de polluer, je sauve ma Terre! </i>
			<span class = "glyphicon glyphicon-envelope"><br/><br/> <a class = "lien" href = "mailto:ceni@uqam.ca"> ceni@uqam.ca </a> </span>
			<span class = "glyphicon glyphicon-earphone"><br/><br/> 514.001.0023 </span>
			<span class = "glyphicon glyphicon-globe"> <br/> 0102 Rue Terre-Vivante <br/> T2M1C0 </span>
			<span class = "glyphicon glyphicon-book"> <br/><br/> <a class= "lien" href = "../html/pageAcceuil.html">  Francais  </a> 
			<a class="lien" href = "../html/anglais.html">  Anglais  </a></span>
			
		</header>
		<br/>
		
		<nav role="navigation">
		
		<ul class="barre">
			<li><a href="../html/contacts.html">Contacts</a></li>
			<li><a href="../html/enrolement.html">Enrolement</a></li>
			<li><a href="../html/connexion.html">Connexion</a></li>
			<li><a href="../html/carteElec.html">Carte Electorale</a></li>
			<li><a href="../html/publication.html">Publications</a></li>
			<li><a href="../html/partis.html">Partis</a></li>
			<li><a href="../php/candidats.php">Candidats</a></li>
			<li><a href="#!">Elections</a>
			<ul class="dropdown">
				<li><a href="../html/electionPresident.html">electionPresidentielle</a></li> 
				<li><a href="../html/electionFederal.html">electionFederale</a></li> 
				<li><a href="../html/electionProvincial.html">electionProvinciale</a></li>
			</ul>
			</li>
		</ul>
		
		</nav>
		
    <div class="container">

<?php 
require 'config.php';

class Electeurs{
    const ECHEC = 0;
    const SUCCES = 1;
    
    public function enregistrerForm() {
        
        global  $database;
        $infoForm = array();
        
        if(!isset($_REQUEST["soumettre"])){
            return self::PAS_DE_FORMULAIRE;
        }
        
        $infoForm["prenom"] = $_REQUEST["prenom"];
        $infoForm["nom"] = $_REQUEST["nom"];
        $infoForm["telephone"] = $_REQUEST["telephone"];
        $infoForm["courriel"] = $_REQUEST["courriel"];
        $infoForm["adresse"] = $_REQUEST["adresse"];
        $infoForm["commentaire"] = $_REQUEST["commentaire"];
        
        // ici on a deja collect� toutes informations du formulaire
        // on va enregistrer dans la BD
        
        $insertionOk = $database->insert("electeurs",$infoForm);
        if($insertionOk){
            return self::SUCCES;
            echo "<h3 class='text-success'>Enregistr� avec succ�s!</h3>";
        }else{
            echo "<h3 class='text-danger'>Echec enregistrement base de donn�es</h3>";
            return self::ECHEC;
        }
        
    }
    
    public function afficherTout() {
        global  $database;
        $enregistrements = $database->select("electeurs", [
            "prenom",
            "nom",
            "telephone",
            "courriel",
            "adresse",
            "commentaire"
        ]);
        
        echo "<h3>Liste des enregistrements</h2>";
        echo "<div class='container'>";
        echo "<table  id='tabElect' class='display' style='width:100%'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Prenom</th>";
        echo "<th>Nom</th>";
        echo "<th>Telephone</th>";
        echo "<th>Courriel</th>";
        echo "<th>Adresse</th>";
        echo "<th>Commentaire</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach ($enregistrements as $enregistrement){
            echo "<tr>";
            echo "<td>".$enregistrement["prenom"]."</td>";
            echo "<td>".$enregistrement["nom"]."</td>";
            echo "<td>".$enregistrement["telephone"]."</td>";
            echo "<td>".$enregistrement["courriel"]."</td>";
            echo "<td>".$enregistrement["adresse"]."</td>";
            echo "<td>".$enregistrement["commentaire"]."</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
        
    }
}

$elect = new Electeurs;
$elect->enregistrerForm();
$elect->afficherTout();

require 'tail.php';