/**
 * @author Johnny Tsheke
 *
 *Modifier par M.M
 */

$(document).ready(function(){
	$("#circonscription").click(function(){
		$("#affiche").toggle();
		$("#affiche").jstree({
			'core':{
				'data':{
					'url':function(node) { 
					 return node.id === "#" ?
					 '../JS/circonscriptions.json'://url AJAX si racine
					 '../JS/circonscriptions.json'; //url AJAX sinon	
					},
					'data': function (node) {
						return{'id': node.id};
					},
					'dataType':'json'
				}
			}
		});
	});
});